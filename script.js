var canvas = document.getElementById("canvas");
var ctx = canvas.getContext("2d");
var sec = 0;
var min = 0;
var time = 0;
var told = 0;
timeup = 0;
var answered = 0;
var clue = [
  "Check the punctuation!",
  "Solve b first.",
  "Think of it as two seperate houses.",
  "Think of how many holes a shirt normally has.",
  "It says they're $1.00 APART not that one of them is $1.00.",
  "The answer has six letters.",
];
var riddles = [
  0,
  "A cat has 3 kittens: One, Two, and Three. What is the cat's name.",
  " ",
  "A man buys a house for 1 million dollars. He sells the house for 1.1 million dollars. Then, he buys it back for 1.2 million dollars. He sells it again for 1.3 million dollars. How much money did he earn/lose? Answer in millions.",
  "How many holes does this t-shirt have?",
  "A bat and a baseball cost $1.10. If the bat costs one more dollar than the baseball, how much does the bat cost? Answer like this: $_._ _",
  "The one who makes it always sells it. \n The one who buys it never uses it.\n The one who uses it never knows he's using it.\n It is a ______.",
];
var remclu = 3;
var clueUsed = 0;
var answer = ["WHAT", 74658, 0.2, 8, "$1.05", "COFFIN"];
var roomNum = 1;
var riddle1 = new Image();
riddle1.src = "img/kitten.png";
var riddle2 = new Image();
riddle2.src = "img/Riddles_Briefcase.png";
var riddle3 = new Image();
riddle3.src = "img/ouse_gabrielle.png";
var riddle4 = new Image();
riddle4.src = "img/3ImageOther-article.jpg";
var riddle5 = new Image();
riddle5.src = "img/baseball.jpg";
var riddle6 = new Image();
riddle6.src = "img/man.gif";
var riddle7 = new Image();
riddle7.src = "img/congrats.jpg";

function addsec() {
  if (min == 30 && told == 0) {
    told = 1;
    alert("Times up!");
    timeup = 1;
  }
  if (answered != 1 && told == 0) {
    sec++;
    told = 0;
    if (sec == 60) {
      min++;
      sec = 0;
    }
    if (sec < 10) {
      time = min + ":0" + sec;
    }
    if (sec > 9) {
      time = min + ":" + sec;
    }
    document.getElementById("hiii").innerHTML = time;
  }
}

//using functions/other
setInterval(addsec, 1000);

canvas.width = 480;
canvas.height = 320;
ctx.drawImage(riddle1, 0, 0, 480, 320);
document.getElementById("riddle").innerHTML = riddles[roomNum];
//define functions:
function checkclue() {
  if (clueUsed == 1) {
    alert("You have already used your hint for this quiz.");
  } else {
    if (remclu > 0) {
      if (
        confirm(
          'Are you sure you want to use a hint? Doing so will take away one of your remaining clues. Press "Okay" to continue or press "Cancel"to cancel.'
        )
      ) {
        remclu--;
        clueUsed = 1;
        document.getElementById("clueMessage").innerHTML = clue[roomNum - 1];
        if (remclu == 1) {
          document.getElementById("remainingclues").innerHTML =
            "You have 1 clue left.";
        } else {
          document.getElementById("remainingclues").innerHTML =
            "You have " + remclu + " clues left.";
        }
      }
    } else {
      alert("You have no clues left!");
    }
  }
}
function ent() {
  if (timeup == 0) {
    var str = document.getElementById("INPUT").value;
    if (str.toUpperCase() == answer[roomNum - 1] && roomNum < 6) {
      alert("Great job! You made it to the next quiz...");
      roomNum++;
      document.getElementById("title").innerHTML = "Viking Quiz #" + roomNum;
      changeCanvas();
    } else if (roomNum < 6) {
      alert("Sorry, wrong answer!");
    } else {
      changeCanvas();
      alert(
        "You finished at a total of " +
          min +
          " minutes and " +
          sec +
          " seconds!"
      );
      answered = 1;
      document.getElementById("title").innerHTML = "Congratulations!";
      document.getElementById("riddle").innerHTML =
        "Your Viking team finished Successfully";
      document.getElementById("inputSection").remove();
      canvas.width = 400;
      canvas.height = 400;
      ctx.clearRect(0, 0, canvas.width, canvas.height);
      ctx.drawImage(riddle7, 0, 0, 343.7, 290);
    }
  }
}
function help() {
  alert(
    'Rules:\n- Click "ENTER" button to submit your answer\n- Click "OK" to go to the next quiz or to confirm using a hint\n- Click clue for a hint\n- No spaces!!! There should be no need for them.'
  );
}
function changeCanvas() {
  clueUsed = 0;
  document.getElementById("INPUT").value = "";
  document.getElementById("clueMessage").innerHTML = "";
  ctx.clearRect(0, 0, canvas.width, canvas.height);
  if (roomNum == 2) {
    canvas.width = 800;
    canvas.height = 600;
    ctx.drawImage(riddle2, 0, 0, 800, 600);
  }
  if (roomNum == 3) {
    canvas.width = 300;
    canvas.height = 300;
    ctx.drawImage(riddle3, 0, 0, 300, 300);
  }
  if (roomNum == 4) {
    canvas.width = 350;
    canvas.height = 300;
    ctx.drawImage(riddle4, 0, 0, 350, 300);
  }
  if (roomNum == 5) {
    canvas.width = 600;
    canvas.height = 150;
    ctx.drawImage(riddle5, 0, 0, 600, 150);
  }
  if (roomNum == 6) {
    canvas.width = 300;
    canvas.height = 300;
    ctx.drawImage(riddle6, 0, 0, 300, 300);
  }

  document.getElementById("riddle").innerHTML = riddles[roomNum];
}
help();
